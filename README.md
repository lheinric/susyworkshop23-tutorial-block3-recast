# SUSYWorkshop23-Tutorial-Block3-RECAST

[![Open in SWAN][image]][hyperlink]

[image]: open_in_swan.svg

[hyperlink]: https://swan-k8s.cern.ch/user-redirect/download?projurl=https://gitlab.cern.ch/lheinric/susyworkshop23-tutorial-block3-recast.git



This part of the tutorial builds on the previous part https://gitlab.cern.ch/ekourlit/susyworkshop23-tutorial-block3 

That part created the basic configuration of the statistical analysis for the analysis.


In this tutorial we will look at simulating RECASTing the analysis

<img src="assets/RECASTOverview.png" style="width:1000px;">

There are two notebooks in this notebook:

1. CreatePatch.ipynb

This notebook shows how we can use the create a "likelihood patch file" using`cabinetry` from a ntuple that corresponds to a new signal, which is assumed to have been created using the code in Block 1 of the analysis


2. PatchAndRecast.ipynb

This notebook shows how we can use the BkgOnly model from the patch file from the first notebook to
create a reinterpreted statistical result.


