{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "import yaml\n",
    "import cabinetry\n",
    "cabinetry.set_logging()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# RECASTing your Analysis\n",
    "\n",
    "RECASTing your analysis is about maximizing the impact your analysis has in the future, such as contributing to pMSSM scans.\n",
    "\n",
    "The core idea is that the phasespace that your analysis covers is not only sensitive to the\n",
    "the signal model that your analysis is targeting but also to many other models of new physics.\n",
    "\n",
    "That means we can not only make a scientific statement about the status of your main signal models\n",
    "but later on also run new signal models through your analysis.\n",
    "\n",
    "The nice things about RECAST is that most of the hard things that go into an analysis\n",
    "are fixed:\n",
    "\n",
    "* the data is taken and processed\n",
    "* all standard model backgrounds are estimated and a background model has been created (Block 2)\n",
    "* the phasespace regions of the analysis (cuts etc) are fixed (Block 1)\n",
    "* the statistical modelling is fixed as well (Nuisance Parameters etc Block 3 Part 1)\n",
    "\n",
    "That means to reinterpret the analysis we only need to do the following steps\n",
    "\n",
    "* Generate a new Signal in the form of Derivations (centrally using MC and Reconstruction requests)\n",
    "* Run the new signal through the event selection to get yields (the \"patch\")\n",
    "* Modify the Statistical Workspace (add the new signal patch to the bkg only workspace)\n",
    "* Re-run the fit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"assets/RECASTOverview.png\" style=\"width:1000px;\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Copy the new Signal Ntuple (produced like in Block 1)\n",
    "\n",
    "Uncomment the below line and copy a new signal to your local work area"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [],
   "source": [
    "# !mkdir data\n",
    "# !cp -r /eos/atlas/atlascerngroupdisk/phys-susy/AnalysisChallenge2023/SqSq_Hplus_1000_500.root data/recast_ntuple_SqSq_Hplus_1000_500.root\n",
    "# ls -lrt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now write to functions\n",
    "\n",
    "1. `create_config_for_recast` \n",
    "\n",
    "This will create on the fly a `cabinetry` config that will apply the same signal and control\n",
    "region cuts to extract the yields from the ntuple\n",
    "\n",
    "2. `create_recast_patch_from_config` \n",
    "\n",
    "This will use `cabinetry` to build the new signal templates and then create a so-called\n",
    "JSON patch file  [read more about JSONPatch at this link](https://jsonpatch.com)\n",
    "\n",
    "We can use the patch file just like a standard code patch (that e.g. git uses)\n",
    "to modify the Bkg Only Likelihood\n",
    "\n",
    "* Specifically we will add a \"add\" operation that will add the new signal sample to each of the channels in the likelihood\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {
    "extensions": {
     "jupyter_dashboards": {
      "version": 1,
      "views": {
       "default_view": {
        "col": 0,
        "height": 2,
        "row": 0,
        "width": 12
       }
      }
     }
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def create_config_for_recast(base_config, new_signal_ntuple):\n",
    "    config = yaml.safe_load(open(base_config))\n",
    "    config['Samples'][0]['SamplePath'] = new_signal_ntuple\n",
    "    return config\n",
    "\n",
    "def create_recast_patch_from_config(config):\n",
    "    cabinetry.templates.build(config)\n",
    "    ws_builder = cabinetry.workspace.WorkspaceBuilder(config)\n",
    "    channels = ws_builder.channels()\n",
    "    patch = []\n",
    "    for idx,channel_spec in enumerate(channels):\n",
    "        signal_spec = channel_spec['samples'][0]\n",
    "        patch.append({\n",
    "            'op': 'add',\n",
    "            'path': f'/channels/{idx}/samples/0', #add at beginning\n",
    "            'value': signal_spec\n",
    "        })\n",
    "    return patch"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "DEBUG - cabinetry.route -   in region SR\n",
      "DEBUG - cabinetry.route -     reading sample RECAST_Signal\n",
      "DEBUG - cabinetry.route -       variation Nominal\n",
      "DEBUG - cabinetry.histo - saving histogram to histograms/SR_RECAST_Signal.npz\n",
      "DEBUG - cabinetry.route -   in region CR-tty\n",
      "DEBUG - cabinetry.route -     reading sample RECAST_Signal\n",
      "DEBUG - cabinetry.route -       variation Nominal\n",
      "DEBUG - cabinetry.histo - saving histogram to histograms/CR-tty_RECAST_Signal.npz\n",
      "DEBUG - cabinetry.route -   in region CR-Wy\n",
      "DEBUG - cabinetry.route -     reading sample RECAST_Signal\n",
      "DEBUG - cabinetry.route -       variation Nominal\n",
      "DEBUG - cabinetry.histo - saving histogram to histograms/CR-Wy_RECAST_Signal.npz\n",
      "WARNING - cabinetry.histo - the modified histogram histograms/SR_RECAST_Signal_modified.npz does not exist\n",
      "WARNING - cabinetry.histo - loading the un-modified histogram instead!\n",
      "DEBUG - cabinetry.workspace - adding NormFactor mu_Signal to sample RECAST_Signal in region SR\n",
      "WARNING - cabinetry.histo - the modified histogram histograms/CR-tty_RECAST_Signal_modified.npz does not exist\n",
      "WARNING - cabinetry.histo - loading the un-modified histogram instead!\n",
      "DEBUG - cabinetry.workspace - adding NormFactor mu_Signal to sample RECAST_Signal in region CR-tty\n",
      "WARNING - cabinetry.histo - the modified histogram histograms/CR-Wy_RECAST_Signal_modified.npz does not exist\n",
      "WARNING - cabinetry.histo - loading the un-modified histogram instead!\n",
      "DEBUG - cabinetry.workspace - adding NormFactor mu_Signal to sample RECAST_Signal in region CR-Wy\n"
     ]
    }
   ],
   "source": [
    "base_config = 'config_recast.yml'\n",
    "sample_path = 'recast_ntuple_SqSq_Hplus_1000_500.root'\n",
    "config_for_recast = create_config_for_recast(base_config, sample_path)\n",
    "patch = create_recast_patch_from_config(config_for_recast)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Look at the Patch\n",
    "\n",
    "As you can see, the likelihood has now three \"add\" operations, one for each Analysis Region (one SR and two CRs),\n",
    "where we are adding the new signal.\n",
    "\n",
    "Note that it's important to add the signal also to the control regions as the new signal\n",
    "might not be totally contained in the SR (which were designed for the original model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'op': 'add',\n",
       "  'path': '/channels/0/samples/0',\n",
       "  'value': {'name': 'RECAST_Signal',\n",
       "   'data': [37.255777295678854],\n",
       "   'modifiers': [{'name': 'staterror_SR',\n",
       "     'type': 'staterror',\n",
       "     'data': [1.4461148054793227]},\n",
       "    {'data': None, 'name': 'mu_Signal', 'type': 'normfactor'}]}},\n",
       " {'op': 'add',\n",
       "  'path': '/channels/1/samples/0',\n",
       "  'value': {'name': 'RECAST_Signal',\n",
       "   'data': [47.77777725830674],\n",
       "   'modifiers': [{'name': 'staterror_CR-tty',\n",
       "     'type': 'staterror',\n",
       "     'data': [1.6360578032011879]},\n",
       "    {'data': None, 'name': 'mu_Signal', 'type': 'normfactor'}]}},\n",
       " {'op': 'add',\n",
       "  'path': '/channels/2/samples/0',\n",
       "  'value': {'name': 'RECAST_Signal',\n",
       "   'data': [14.600712824612856],\n",
       "   'modifiers': [{'name': 'staterror_CR-Wy',\n",
       "     'type': 'staterror',\n",
       "     'data': [0.8994155060658561]},\n",
       "    {'data': None, 'name': 'mu_Signal', 'type': 'normfactor'}]}}]"
      ]
     },
     "execution_count": 49,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "patch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save the Patch\n",
    "\n",
    "We will now save the patch into a JSON file, which we can pass on to the statistical analysis \n",
    "in the `PatchAndRecast` notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 50,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('patch_from_cabinetry.json','w') as patchfile:\n",
    "    json.dump(patch,patchfile)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "extensions": {
   "jupyter_dashboards": {
    "activeView": "default_view",
    "version": 1,
    "views": {
     "default_view": {
      "cellMargin": 10,
      "defaultCellHeight": 40,
      "maxColumns": 12,
      "name": "active_view",
      "type": "grid"
     }
    }
   }
  },
  "kernelspec": {
   "display_name": "Python 3.11.4 64-bit ('garbargeml')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "2ef91e534abe79302bcf2b7f1b029f2935bc4c6fa03da29571980ae05169f629"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
